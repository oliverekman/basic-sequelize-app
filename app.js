const { Sequelize, QueryTypes } = require('sequelize');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const sequelize = new Sequelize({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
});

async function connect() {
    try {
        await sequelize.authenticate();
        console.log('Connection successfull');
        const cities = await sequelize.query('SELECT * from city LIMIT 10', { type: QueryTypes.SELECT });
        const countries = await sequelize.query('SELECT * from country LIMIT 10', { type: QueryTypes.SELECT });
        const languages = await sequelize.query('SELECT * from countrylanguage LIMIT 10', { type: QueryTypes.SELECT })
        console.log(cities);
        console.log(countries);
        console.log(languages);
        await sequelize.close();
    }
    catch (e) {
        console.log(e);
    }
}

connect();